﻿#include <iostream>
#include <vector>

int main()
{
	std::vector<int> myVector;

	for (int count = 0; count < 5; ++count)
		myVector.push_back(count);

	std::vector<int>::const_iterator it; // объявляем итератор только для чтения

	it = myVector.begin(); // присваиваем ему начальный элемент вектора

	while (it != myVector.end()) // пока  итератор не достигнет последнего элемента
	{
		std::cout << *it << " "; // выводим значение элемента, на который указывает итератор
		++it; // и переходим к следующему элементу
	}
}